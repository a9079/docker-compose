FROM python:3.6

COPY . .

RUN pip3 install -r requirements.txt &&\

                     
                     chmod +x entrypoint.sh

EXPOSE 5000

ENTRYPOINT  ["./entrypoint.sh"]




